import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../pokemon';
import {PokeApiService} from '../pokemon.service';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.scss']
})
export class MyComponentComponent implements OnInit {

  constructor(private pokeApiService: PokeApiService) { }

  idPokemon = '';
  filtrePokemon = '';
  pokemons: Pokemon[];

  // POKEMONS: Array<Pokemon> = [
  //   new Pokemon(1, 'Bulbizarre'),
  //   new Pokemon(2, 'Herbizarre'),
  //   new Pokemon(3, 'Florizarre'),
  //   new Pokemon(4, 'Salameche'),
  //   new Pokemon(5, 'Reptincel'),
  //   new Pokemon(6, 'Dracaufeu')
  // ];

  go() {
    console.log('Pokemon selectionné :', this.idPokemon);
  }

  ngOnInit() {
    this.getPokemons();
  }

  getPokemons(): void {
    this.pokeApiService.getPokemons()
      .subscribe(pokemons => console.log(pokemons.));
  }

}
