import { Injectable } from '@angular/core';
import {Pokemon} from './pokemon';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class PokeApiService {

  constructor(private http: HttpClient) { }

  private urlPokeApi = 'https://pokeapi.co/api/v2/';

  getPokemons(): Promise<Pokemon[]> {
    return this.http.get(`${this.urlPokeApi}pokemon`)
      .toPromise()
      .then(response => {
        return response.
          .results
          .map((pokemon) => Pokemon.parse(pokemon))
      })
      .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred:', error.statusText);
    return Promise.reject(error.statusText || error);
  }
}
